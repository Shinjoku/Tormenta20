# Lista de Mudanças
## Versão 1.1.56
### Consertos de Bugs
- Ficha NPC: Listas de perícias aparecem fora do modo de edição.

## Versão 1.1.55
### Novidades
- Agora todas as ameaças têm tokens.

### Consertos de Bugs
- Ameaças: Ajustado ataques de algumas criaturas e adicionado habilidades inatas dos aberrantes e mortos-vivos.
- Poções: Adicionado peso para as poções (0,5 kg).

## Versão 1.1.54
### Novidades
- Nome de habilidades do NPC indicam se a habilidade é Sustentada.
- Adicionado suporte ao módulo Bug Reporter. Agora é possível reportar bugs diretamente de dentro do Foundry, sem precisar criar uma conta no Gitlab, basta instalar o módulo.
- Ficha NPC: Listas de perícias ou equipamentos vazios são ocultados.
- Ficha NPC: Armas naturais não aparecem na lista de equipamentos fora do modo de edição.

### Consertos de Bugs
- Ameaças: Glop corrigido (estava com visão no escuro e sem imunidades).

## Versão 1.1.53
### Novidades
- A descrição de dano das armas nas fichas de personagens agora não faz cálculos, mostrando a fórmula atual do dano.

### Consertos de Bugs
- Lobo-Crocodilo não tem mais deslocamento de voo.

## Versão 1.1.52
### Novidades
- Ficha NPC: Campos de descrição Corpo a Corpo e À Distância adicionados. Agora é possíve colocar coisas como "2 garras" no NPC.
- Ficha NPC: Itens são listados na seção de Equipamentos.
- Ameaças: Descrição Corpo a Corpo e À Distância adicionados a todas as ameaças.

![image](/uploads/a6e1dd6fee00c5c7e39dcc3d38c479ba/image.png)

### Consertos de Bugs
- Rolagens com armas sem dano (ex: Rede) funcionam normalmente.
- Armas sem dano (ex: Rede) não mostram mais o dano na descrição em fichas de personagens.

## Versão 1.1.51
### Consertos de Bugs
- Ficha NPC: Conserto de um bug que não deixava fichas de NPCs serem abertas.

## Versão 1.1.50
### Novidades
- Fichas: O total da CD base das magias (10 + metade do nível + atributo-chave) agora é mostrado nas fichas.

### Consertos de Bugs
- NPCs: Imagens não são substituídas pelo padrão ao duplicar ou importar NPCs.

## Versão 1.1.49
### Novidades
- Ameaças: Tokens adicionados a algumas criaturas.
- Ameaças: Centauro e Centauxo Xamã adicionados.

## Versão 1.1.48
### Novidades
- Ficha PJ: Fichas são criadas com o modo de edição de perícias ligado.
- NPC: o botão Editar altera a ficha sem abrir uma segunda janela.
- Ameaças: Visão adicionada a todas as ameaças.

## Versão 1.1.47
### Novidades
- Poções: Novas cores para Óleos e Granadas
- Mais ícones para itens.

### Consertos de Bugs
- Ameaças: É possível puxar ameaças do compêndio novamente.

## Versão 1.1.46
### Novidades
- As seguintes magias foram atualizadas: Adaga Mental, Amarras Etéreas, Anular a Luz, Controlar Fogo, Controlar Madeira, Legião, Roubar a Alma, Santuário e Transmutar Objetos.
- As seguintes condições foram atualizadas: Enjoado e Sangrando.
- Magias: Aprimoramento de 12 PM da magia Augúrio adicionado.
- Magias e Poderes: Nomes de magias agora estão em itálico.
- Poderes: Pré-requisitos agora estão em itálico.

## Versão 1.1.45
### Novidades
- Ameaças: Nomes de algumas criaturas trocados para versões OGL: Arauto dos Goblinóides, Sombra dos Goblinóides, Cultista da Traição, Supremacistas (Recruta, Soldado, e Cavaleiro) , Lobo-Crocodilo, Aberrantes (Formiga, Formiga Maior, Besouro e Assassino).
- Ameaças: 3 variantes de Troll adicionadas.

### Consertos de Bugs
- Deletar uma armadura ou escudo equipado atualiza a Defesa corretamente.

## Versão 1.1.44
### Novidades
- Novos ícones para vários itens.

## Versão 1.1.43
### Consertos de Bugs
- Propriedades adicionadas à Katana.

## Versão 1.1.42
### Consertos de Bugs
- Equipamentos que não eram armaduras leves/pesadas ou escudos não removiam seu bônus ao serem deletados.
- Poções: Preços estão no lugar certo, não no custo em PM.
- Armas: Atributo pode ser selecionado em armas de NPCs.
- Maça de guerra, Cajado de batalha e batata ambiciosa movidos de volta para o compêndio de Equipamentos.

## Versão 1.1.41
### Consertos de Bugs
- Armaduras: Enquanto equipado com uma armadura natural ou traje, equipar outro do mesmo tipo não subtraía o bônus do item desequipado.
- Armaduras: Personagens podem equipar mais de uma Armadura Natural (exemplo: Couro Rígido e Casca Grossa).
- Poderes: Inspirar Glória (poder de Nobre) adicionado.
- Nome de alguns poderes consertados.

## Versão 1.1.40
### Consertos de Bugs
- Armas: O atributo certo será adicionado à rolagem de ataque.
- Ícones da Língua do Deserto e Baluarte Anão aparecem normalmente.
- Poções: Preços adicionados.

## Versão 1.1.39
### Novidades
- Novos ícones para Equipamentos.

## Versão 1.1.38
### Novidades
- Compêndio: Poções adicionadas.
- Compêndio: Itens Mágicos separados dos Equipamentos.
- Ficha PJ: Fonte do Deslocamento aumentada.
- ~~Novos ícones para Equipamentos.~~

### Consertos de Bugs
- Chat: A descrição de Consumíveis aparece normalmente.
- A magia Chuva de Meteoro causa o dano corretamente (10d6 + 10d6 de dano em vez de 20d6).
- Navegador de Compêndio: filtros de Execução das magias e de Subtipo de poderes consertado.
- Compêndio: Alguns escudos estavam configurados como Armaduras em vez de Escudos.
- Compêndio: Gema Elemental adicionada.
- Ficha NPC: Deslocamento de Voo aparece corretamente em vez de mostrar o deslocamento normal.

## Versão 1.1.37
### Novidades
- Adicionado Menu de Deslocamento. Agora é possível adicionar outros tipos de deslocamento às fichas. NPCs ficarão com um campo "Deslocamento (Antigo)" até que a informação seja apagada.
- Compêndio de Perícias. Passe o mouse do lado direito do nome de uma perícia e um ícone de livro aparecerá, clicar nele abrirá a entrada de diário com as informações da perícia.
- Itens, magias e poderes não têm mais hífens no meio das palavras.
- Suporte ao módulo Drag Ruler adicionado.

### Consertos de Bugs
- A magia Sopro das Uivantes agora tem rolagem de dano.
- Aprimoramentos da magia Luz consertados.

## Versão 1.1.36
### Consertos de Bugs
- Caixas de descrição vazias (por exemplo, de itens recém-criados) podem ser editadas.

## Versão 1.1.35
### Consertos de Bugs
- Personagens com Bônus de PV/PM não definidos conseguem arrastar classes para a ficha.
- As caixas de seleção de Atributos nas configurações de nível não se remarcam após serem desmarcadas.

## Versão 1.1.34
### Novidades
- Favoritos adicionados à ficha de abas. Clique na estrela em um item, poder ou magia e ela aparecerá na aba Atributos.
- Automatização dos PV/PM. Adicionar/remover uma classe irá mudar os PV e PM do personagem. A primeira classe adicionada será considerada como o primeiro nível para cálculos de PV. Detalhe: alterar o valor `Níveis de Classe` dentro de uma Classe não irá alterar os valores.
- Clicar no Nível abre um menu para adicionar PV e PM extras. Detalhe: a habilidade do Anão, Duro como Pedra, deve ser adicionada como Bônus Total 2 e Bônus por Nível 1 para calcular corretamente.
- Adicionados mais idiomas do TRPG à lista de idiomas.
- Biografia e Diário unificados.
- Opção para o Mestre desabilitar o Diário (somente a biografia aparecerá).
- Melhorias visuais gerais.
- NPC: Idiomas e Dinheiro podem ser adicionados à ficha.
- Armas: Atributos podem ser escolhidos para a rolagem das armas, independente do atributo da Perícia escolhida. Detalhe: Sempre foi possível adicionar outros atributos adicionando `@for/des/con/int/sab/car` como Bônus.
- Armas: Atributos e Perícias não aparecem para NPCs. Mudança provisória.
- Compêndio: Armas como mordidas, garras, etc agora são consideradas armas naturais.
- Compêndio: Magia Preparação de Batalha atualizada.

### Consertos de Bugs
- Editar uma descrição não irá fazer com que o texto desapareça.
- As perícias das Classes podem ser selecionadas pelos jogadores.

## Versão 1.1.33
### Novidades
- As perícias das Classes podem ser modificadas pelo Mestre.
- Novas Configurações de Personagem adicionadas (menu de engrenagens na ficha do Personagem).
- BUG: As perícias das Classes não podem ser selecionadas.

## Versão 1.1.32
### Novidades
- Adicionadas Classes. Vá nos Pacotes de Compêndio e adicione uma classe ao seu personagem.
